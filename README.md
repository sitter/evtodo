Generates a text dump of open KDE e.V. tasks.

- `ruby evtodo`
- find evtodo.txt as textual dump file

invent.kde.org creates a file daily at https://invent.kde.org/sitter/evtodo/-/jobs/artifacts/master/raw/evtodo.txt?job=evtodo
